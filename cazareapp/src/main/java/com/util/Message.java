package com.util;

public class Message {

	private String message;
	private int currentNumber;

	public Message(int nr, String msg) {
		message = msg;
		currentNumber = nr;
	}

	public String getMessage() {
		return message;
	}

	public int getCurrentNumber() {
		return currentNumber;
	}
	public void setMessage(String msg){
		message=msg;
	}
	public void setNumber(int nr){
		currentNumber=nr;
	}
}
