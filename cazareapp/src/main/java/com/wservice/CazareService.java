package com.wservice;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import com.util.MessageStoreUtil;

/**
 * 
 * @author Catalin Moldovan
 *
 */

@Path("/status")
public class CazareService {
	

	@POST
	@Path("/addmessage")
	public String postm(@HeaderParam("number") int currentNumber,
			@HeaderParam("message") String message) {
		MessageStoreUtil.lastMessage.setMessage(message);
		MessageStoreUtil.lastMessage.setNumber(currentNumber);
		return "Message Added";
	}

	@GET
	@Path("/getmessage")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMessage() {
		JSONObject response = new JSONObject();
		try {
			response.put("number", MessageStoreUtil.lastMessage.getCurrentNumber());
			response.put("message", MessageStoreUtil.lastMessage.getMessage());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return Response.ok(response, MediaType.APPLICATION_JSON).build();
	}
}
