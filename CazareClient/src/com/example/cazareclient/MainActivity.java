package com.example.cazareclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

import android.support.v7.app.ActionBarActivity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Button getData=(Button) findViewById(R.id.getservicedata);
		getData.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
						String restURL="http://www.androidexample.com/media/webservice/JsonReturn.php";	
					new RestOperation().execute(restURL);	
			}
		});
		
	}

	public class RestOperation extends AsyncTask<String,Void,Void>{
		final HttpClient httpClient=new DefaultHttpClient();
		String content;
		String error;
		TextView dataReceived=(TextView) findViewById(R.id.textView1);
		ProgressDialog progressDialog=new ProgressDialog(MainActivity.this);
	
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
		progressDialog.setTitle("Please wait...");
		progressDialog.show();
		}
		@Override
		protected Void doInBackground(String... params) {
			// TODO Auto-generated method stub
			URL url;
			BufferedReader br=null;
			try {
				url=new URL(params[0]);
				try {
					URLConnection connection=url.openConnection();
			br=new BufferedReader(new InputStreamReader(connection.getInputStream()));
			StringBuilder sb=new StringBuilder();
			String line=null;
			
			while((line= br.readLine())!=null){
				sb.append(line);
				sb.append(System.getProperty("line.separator"));
			}
			content=sb.toString();
				} catch (IOException e) {
				error=e.getMessage();
					e.printStackTrace();
				}
				
			} catch (MalformedURLException e) {
				error=e.getMessage();
				e.printStackTrace();
			}finally{
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			return null;
		}
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		progressDialog.dismiss();
		if(error!=null){
			dataReceived.setText("Error: "+error);
		}else{
			dataReceived.setText(content);
		}
	}
	}


}
