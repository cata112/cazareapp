package com.example.cazarecamin7;
import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.TextureView;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {
	TextView currentNumber,mainMessage;
	boolean launched=false;
	private String savedMessage,message;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		currentNumber=(TextView) findViewById(R.id.textView1);
		mainMessage=(TextView) findViewById(R.id.textViewMesaj);
		
		if (savedInstanceState!=null){
			message=savedInstanceState.getString(savedMessage);
			mainMessage.setText(message);
			
		}else
		{
		mainMessage.setText("");
		}
		
	}
	public void getInfo(View v){
		message="Buton apasat";
		mainMessage.setText(message);
		
	}
	
	@Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        
        outState.putString(savedMessage, message);
    }
	
}
